/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/02 19:04:29 by vincent           #+#    #+#             */
/*   Updated: 2014/01/18 16:05:30 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <sys/types.h>
# include <sys/uio.h>
# include <stdlib.h>

# define BUFF_SIZE 500
# define ENDL	ft_strnstr(buf + i.i, "\n", i.ret - i.i)

int					get_next_line(int const fd, char **line);

typedef struct	s_str_prop
{
	int		char_rd;
	char	*file;
	char	*tmp;
}				t_str_prop;

typedef	struct			s_counter
{
	int					i;
	int					ret;
}						t_counter;

#endif
