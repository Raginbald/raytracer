/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/18 11:37:02 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 02:58:27 by vincent          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RT_H
# define RT_H

# define USAGE	"usage : ./rt scene"

/*
** W_NAME		window's name
** WIDTH 		horizontal resolution
*/

# define KP 			2
# define KR				3
# define KPM			(1L<<0)
# define KRM			(1L<<1)
# define W_NAME			"poor RT"

# define DIFFUSE		1
# define FOV			33
# define X				t.x
# define Y				t.y
# define PRIM_NB		4
# define DEPTH_MAX		1

# include <vectors.h>
# include <scene.h>
# include <matrix.h>
# include <get_next_line.h>

/*
** OBJECTS TYPE
**
** sphere : 0
*/

/*
**		STRUCT ENVIRONMENT
**
** pf : void pointer on t_pfunc
*/

typedef struct		s_env
{
	void		*mlx;
	void		*win;
	void		*img_ptr;
	char		*img;
	int			bpp;
	int			size_line;
	int			endian;
	t_cam		cam;
	t_obj		*obj;
	t_light		*light;
	int			obj_nb;
	int			light_nb;
	t_3d		ambient;
	void		*pf;
}					t_env;


# include <func_struct.h>
# include <init.h>
# include <calc.h>

/*
** Environment mangement and drawing
*/

int			draw_hook(t_env *e);
void		draw_img_pixel(t_env *e, int x, int y, int color);
void		draw_test(t_env *e, int x);
int			expose_window(t_env *e);

/*
** Movement
*/

int			key_press_event(int keycode, t_env *e);
int			key_release_event(int keycode, t_env *e);

/*
** Tools functions
*/

double	below_one(double d);
void	rotate_point(t_obj *obj, t_3d *ori, t_3d *new);
void	translate_point(t_3d *ori, t_3d *point, t_3d *new);
void	rotate_point_inverse(t_obj *obj, t_3d *ori, t_3d *new);
void	translate_point_inverse(t_3d *ori, t_3d *point, t_3d *new);
void	cpy_vector(t_3d *ori, t_3d *cpy);

void	check_params(t_env e);
void	print_matrix(t_mat4 m);
void	disp_vec(t_3d v);
void	draw_light(int *on, char *s);

#endif
