/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vectors.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vvaleriu <vvaleriu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/18 11:33:26 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/18 12:48:49 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VECTORS_H
# define VECTORS_H

/*
**		VECTORS
*/

typedef struct			s_2d_coord
{
	int		x;
	int		y;
}						t_2d_coord;

typedef struct			s_2d
{
	double	x;
	double	y;
}						t_2d;

typedef struct			s_3d
{
	double	x;
	double	y;
	double	z;
}						t_3d;


void	vec_sum(t_3d *p1, t_3d *p2, t_3d *p3);
void	vec_sub(t_3d *p1, t_3d *p2, t_3d *p3);
double	vec_norm(t_3d v);
void	vec_normalise(t_3d *v);
double	vec_dotprod(t_3d a, t_3d b);
void	vec_crossprod(t_3d *a, t_3d *b, t_3d *c);
void	vec_prod(t_3d *a, double i, t_3d *c);
void	vec_multiply(t_3d *a, t_3d *b, t_3d *c);

#endif
