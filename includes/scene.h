/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scene.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/18 12:29:46 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/25 22:49:21 by vincent          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCENE_H
# define SCENE_H

# include <vectors.h>
# include <matrix.h>

# define ECPX			e->cam.pos.x
# define ECPY			e->cam.pos.y
# define ECPZ			e->cam.pos.z
# define ECPVX			e->cam.pos_v.x
# define ECPVY			e->cam.pos_v.y
# define ECPVZ			e->cam.pos_v.z
# define RDX			ray->dir.x
# define RDY			ray->dir.y
# define RDZ			ray->dir.z
# define RDVX			ray->dir_v.x
# define RDVY			ray->dir_v.y
# define RDVZ			ray->dir_v.z
# define OPX			obj->pos.x
# define OPY			obj->pos.y
# define OPZ			obj->pos.z

/*
**			DEFINES
**
** V_DIST	: cam - viewplane distance
** V_WIDTH	: viewplane width
** V_HEIGHT : viewplane height
** AR		: ambient red coefficient
** AG		: ambient green coefficient
** AB		: ambient blue coefficient
** DR		: diffuse red coefficient
** DG		: diffuse green coefficient
** DB		: diffuse blue coefficient
** SR		: specular red coefficient
** SG		: specular green coefficient
** SB		: specular blue coefficient
*/

# define COL_TAB_SZ	11
# define V_DIST		cam->v_dist
# define AR			col[0]
# define AG			col[1]
# define AB			col[2]
# define DR			col[3]
# define DG			col[4]
# define DB			col[5]
# define SR			col[6]
# define SG			col[7]
# define SB			col[8]
# define SHINE		col[9]
# define ATT		col[10]

/*
**			STRUCT CAM
** pos			: position vector in the map
** dir			: direction vector of the eye
** up			: up vector of cam plan
** right		: right vector of cam plam
** topleft		: coordonnate of the top left point of the view plane
** pd			: plan distance
** xres / yres	: window width / heigth
** v_dist 		: viewplane distance
** xdisp		: indentation x pour trouver le prochain point
** ydisp		: indentation y pour trouver le prochain point
*/

typedef struct		s_cam
{
	t_3d	pos;
	t_3d	pos_v;
	t_3d	dir;
	t_mat4	mx;
	t_mat4	my;
	t_mat4	mz;
	int		xres;
	int		yres;
	double 	v_dist;
	double	xdisp;
	double	ydisp;
}					t_cam;

/*
**		STRUCT OBJECT
**
** prim type :	see above
** pos		 :	object's gravity's center coordonnates
** dir		 :	object direction (depending on the object type)
** len		 :	object lengh (cones, cylinders, etc)
** radius	 :	object radius of edge size
** col[10]	 :	rgb colors + shininess
** mx		 :	X axis rotation matrix	
** my		 :	Y axis rotation matrix	
** mz		 :	Z axis rotation matrix	
*/

typedef struct		s_obj
{
	int		type;
	t_3d	pos;
	t_3d	dir;
	double	len;
	double	radius;
	double	col[COL_TAB_SZ];
	t_mat4	mx;
	t_mat4	my;
	t_mat4	mz;
	t_mat4	mix;
	t_mat4	miy;
	t_mat4	miz;
}					t_obj;

/*
**		STRUCT LIGHT
**
** type : primitive type, see above
** pos 	: object's gravity's center coordonnates
** col  : light color //pas utilise pour l'instant.
** in  	: light intensity
*/

typedef struct		s_light
{
	int		type;
	t_3d	pos;
	t_3d	dir;
	double	col[COL_TAB_SZ];
}					t_light;

/*
**		STRUCT RAY
**
** pos : ray intersection position
** dir : ray vector
** norm: normale of the ray hit position
** d: distance of the ray from the viewplan
** col: color (rgb structre)
** 
*/

typedef struct		s_ray
{
	t_3d	pos;
	t_3d	dir;
	t_3d	dir_v;
	t_3d	norm;
	double	d;
	double	col[COL_TAB_SZ];
}					t_ray;

#endif
