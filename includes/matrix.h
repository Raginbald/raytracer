/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/18 12:15:15 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 16:10:57 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MATRIX_H
# define MATRIX_H

# include <vectors.h>
# include <scene.h>

/*
** Matrix
*/

# define M00	m[0]
# define M01	m[1]
# define M02	m[2]
# define M03	m[3]
# define M10	m[4]
# define M11	m[5]
# define M12	m[6]
# define M13	m[7]
# define M20	m[8]
# define M21	m[9]
# define M22	m[10]
# define M23	m[11]
# define M30	m[12]
# define M31	m[13]
# define M32	m[14]
# define M33	m[15]

/*
** Tools to calculate rotation matrix
*/

# define A		t[0]
# define B		t[1]
# define C		t[2]
# define D		t[3]
# define E		t[4]
# define F		t[5]
# define AD		t[6]
# define BD		t[7]

# define L11	a->M12 * a->M23 * a->M31 - a->M13 * a->M22 * a->M31 + a->M13 *
# define L12	 a->M21 * a->M32 - a->M11 * a->M23 * a->M32 - a->M12 * a->M21 
# define L13	* a->M33 + a->M11 * a->M22 * a->M33;
# define L21 	a->M03 * a->M22 * a->M31 - a->M02 * a->M23 * a->M31 - a->M03 *
# define L22 	 a->M21 * a->M32 + a->M01 * a->M23 * a->M32 + a->M02 * a->M21 
# define L23 	* a->M33 - a->M01 * a->M22 * a->M33;
# define L31 	a->M02 * a->M13 * a->M31 - a->M03 * a->M12 * a->M31 + a->M03 *
# define L32 	 a->M11 * a->M32 - a->M01 * a->M13 * a->M32 - a->M02 
# define L33 	* a->M11 * a->M33 + a->M01 * a->M12 * a->M33;
# define L41 	a->M03 * a->M12 * a->M21 - a->M02 * a->M13 * a->M21 - a->M03 *
# define L42 	 a->M11 * a->M22 + a->M01 * a->M13 * a->M22 + a->M02 
# define L43 	* a->M11 * a->M23 - a->M01 * a->M12 * a->M23;
# define L51 	a->M13 * a->M22 * a->M30 - a->M12 * a->M23 * a->M30 - a->M13 *
# define L52 	 a->M20 * a->M32 + a->M10 * a->M23 * a->M32 + a->M12 *
# define L53 	 a->M20 * a->M33 - a->M10 * a->M22 * a->M33;
# define L61 	a->M02 * a->M23 * a->M30 - a->M03 * a->M22 * a->M30 + a->M03 *
# define L62 	 a->M20 * a->M32 - a->M00 * a->M23 * a->M32 - a->M02 *
# define L63 	 a->M20 * a->M33 + a->M00 * a->M22 * a->M33;
# define L71 	a->M03 * a->M12 * a->M30 - a->M02 * a->M13 * a->M30 - a->M03 *
# define L72 	 a->M10 * a->M32 + a->M00 * a->M13 * a->M32 + a->M02 *
# define L73	 a->M10 * a->M33 - a->M00 * a->M12 * a->M33;
# define L81	a->M02 * a->M13 * a->M20 - a->M03 * a->M12 * a->M20 + a->M03 *
# define L82	 a->M10 * a->M22 - a->M00 * a->M13 * a->M22 - a->M02 *
# define L83	 a->M10 * a->M23 + a->M00 * a->M12 * a->M23;
# define L91	a->M11 * a->M23 * a->M30 - a->M13 * a->M21 * a->M30 + a->M13 *
# define L92	 a->M20 * a->M31 - a->M10 * a->M23 * a->M31 - a->M11 
# define L93	* a->M20 * a->M33 + a->M10 * a->M21 * a->M33;
# define L101	a->M03 * a->M21 * a->M30 - a->M01 * a->M23 * a->M30 - a->M03 *
# define L102	 a->M20 * a->M31 + a->M00 * a->M23 * a->M31 + a->M01 
# define L103	* a->M20 * a->M33 - a->M00 * a->M21 * a->M33;
# define L111	a->M01 * a->M13 * a->M30 - a->M03 * a->M11 * a->M30 + a->M03 *
# define L112 	 a->M10 * a->M31 - a->M00 * a->M13 * a->M31 - a->M01 *
# define L113 	 a->M10 * a->M33 + a->M00 * a->M11 * a->M33;
# define L121 	a->M03 * a->M11 * a->M20 - a->M01 * a->M13 * a->M20 - a->M03 *
# define L122 	 a->M10 * a->M21 + a->M00 * a->M13 * a->M21 + a->M01 *
# define L123 	 a->M10 * a->M23 - a->M00 * a->M11 * a->M23;
# define L131 	a->M12 * a->M21 * a->M30 - a->M11 * a->M22 * a->M30 - a->M12 *
# define L132 	 a->M20 * a->M31 + a->M10 * a->M22 * a->M31 + a->M11 *
# define L133 	 a->M20 * a->M32 - a->M10 * a->M21 * a->M32;
# define L141 	a->M01 * a->M22 * a->M30 - a->M02 * a->M21 * a->M30 + a->M02 *
# define L142 	 a->M20 * a->M31 - a->M00 * a->M22 * a->M31 - a->M01 *
# define L143 	 a->M20 * a->M32 + a->M00 * a->M21 * a->M32;
# define L151 	a->M02 * a->M11 * a->M30 - a->M01 * a->M12 * a->M30 - a->M02 *
# define L152 	 a->M10 * a->M31 + a->M00 * a->M12 * a->M31 + a->M01 *
# define L153 	 a->M10 * a->M32 - a->M00 * a->M11 * a->M32;
# define L161 	a->M01 * a->M12 * a->M20 - a->M02 * a->M11 * a->M20 + a->M02 *
# define L162 	 a->M10 * a->M21 - a->M00 * a->M12 * a->M21 - a->M01 
# define L163 	* a->M10 * a->M22 + a->M00 * a->M11 * a->M22;

typedef struct		s_mat4
{
	double	m[16];
}					t_mat4;

/*
** ININTIALISE AND FILL ROTATION MATRIX ACCORDING TO ANGLE
*/

void		rot_x_matrix(t_mat4 *m, t_3d ang);
void		rot_y_matrix(t_mat4 *m, t_3d ang);
void		rot_z_matrix(t_mat4 *m, t_3d ang);
void		init_mat(t_mat4 *t);

void		mat_inverse(t_mat4 *m, t_mat4 *i);
void		mat_prod_scalar(t_mat4 *m, double i, t_mat4 *r);
void		mat_prod_vec(t_mat4 m, t_3d *v, t_3d *r);

void		rotation_matrix(t_mat4 *m, t_3d *r);
void		translation_matrix(t_mat4 *m, t_3d *r);
void		transrot_matrix(t_mat4 *, t_3d *r, t_3d *t);
void		print_matrix(t_mat4 m);

#endif
