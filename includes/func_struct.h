/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   func_struct.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vvaleriu <vvaleriu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/18 12:18:30 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/18 12:18:32 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FUNC_STRUCT_H
# define FUNC_STRUCT_H

# include <scene.h>
# include <rt.h>

/*
**		STRUCT PRIMITIVE FUNCTIONS
**
** Structures used to compute primitive coordonates.
*/

typedef struct			s_pfunc
{
	int		nb;
	int		(*f)(t_env *, t_ray *, t_obj *);
}						t_pfunc;

/*
**		STRUCT FUNCTIONS
**
** Functions used to decided wether to parse a light/object or the scene
*/

typedef struct			s_func
{
	char	*s;
	void	(*f)(t_env *, char **, int);
}						t_func;

/*
**		STRUCT OBJECT FUNCTIONS
**
** Functions used to parse object properties
*/

typedef struct			s_ofunc
{
	char	*s;
	void	(*f)(t_obj *, char *);
}						t_ofunc;

/*
**		STRUCT LIGHT FUNCTIONS
**
** functions used to parse light properties
*/

typedef struct			s_lfunc
{
	char	*s;
	void	(*f)(t_light *, char *);
}						t_lfunc;

#endif
