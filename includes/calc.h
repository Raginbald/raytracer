/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calc.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/18 10:34:52 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 01:26:12 by vincent          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CALC_H
# define CALC_H

# define AA		det[0]
# define BB		det[1]
# define CC		det[2]
# define DELTA	det[3]

/*
**		RAYS INITIALISATION AND COMPUTATION
*/

void	cal_vp_point(t_cam *cam, t_3d *p, t_2d_coord t);
int		cal_pixel(t_env *e, t_ray *ray, t_pfunc *f, int	i);
void	init_ray(t_ray *ray, t_3d *origin, t_3d *point);
void	cal_hitpos(t_env *e, t_ray *ray);
void	set_virtual_points(t_cam *cam, t_ray *ray, t_obj *obj);
void	set_original_points(t_3d *p, t_obj *obj);
void	cal_reflected(t_3d *inc, t_3d *norm, t_3d *reflected);

/*
**		INTERSECTION COMPUTATION
*/

t_obj	*cal_intersection(t_env *e, t_ray *ray, t_pfunc *f);
int		cal_sphere_inter(t_env *e, t_ray *ray, t_obj *obj);
void	cal_sphere_normal(t_env *e, t_ray *ray, t_obj *obj);
int		cal_plan_inter(t_env *e, t_ray *ray, t_obj *obj);
void	cal_plan_normal(t_env *e, t_ray *ray, t_obj *obj);
int		cal_cylinder_inter(t_env *e, t_ray *ray, t_obj *obj);
void	delta_above(t_env *e, t_ray *ray, t_obj *obj, double *det);
void	check_cap_intersection(t_env *e, t_ray *ray, t_obj *obj);
int		cal_cone_inter(t_env *e, t_ray *ray, t_obj *obj);
int		cal_rgb(double *col);

/*
**		LIGHT COMPUTATION
*/

void	cal_light(t_env *e, t_ray *ray, t_obj *obj, t_pfunc *pf);
void	init_lightray(t_ray *ray, t_ray *lt_ray, t_light *light);
void	light_general_ambient(t_obj *obj, t_ray *ray, t_3d ambient);
void	light_ambient(t_ray *ray, t_obj *obj, t_light *light);
void	light_diffuse(t_ray *ray, t_ray *lt_ray, t_obj *obj, t_light *light);
void	light_specular(t_ray *ray, t_ray *lt_ray, t_obj *obj, t_light *light);

#endif
