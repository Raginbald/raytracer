/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/18 09:33:50 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/25 00:12:03 by vincent          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INIT_H
# define INIT_H

/*
**		WINDOWS & VIEWPLANE
*/

void		init(t_env *env, char *arg, t_pfunc *pf);
void		init_ptr(t_env *e);
void		cal_viewplane(t_cam *cam);

/*
**		CAM
*/

void		get_cam_origin(t_env *env, char *s);
void		get_res(t_env *env, char *s);
void		get_cam_dir(t_env *env, char *s);
void		get_ambient(t_env *env, char *s);

/*
**		OBJECTS
*/

void		get_obj(t_obj *obj, char **line, int fd);
void		get_obj_dir(t_obj *obj, char *s);
void		get_obj_length(t_obj *obj, char *s);
void		get_obj_material(t_obj *obj, char *s);
void		get_obj_matrix(t_obj *obj);
void		get_obj_number(t_env *env, char *s);
void		get_obj_origin(t_obj *obj, char *s);
void		get_obj_radius(t_obj *obj, char *s);
void		get_obj_type(t_obj *obj, char *s);
void		fill_func_obj(t_ofunc *f);
/*
**		LIGHTS
*/

void		get_light(t_light *light, char **line, int fd);
void		get_light_color(t_light *light, char *s);
void		get_light_dir(t_light *light, char *s);
void		get_light_number(t_env *env, char *s);
void		get_light_origin(t_light *light, char *s);
void		get_light_type(t_light *light, char *s);
void		fill_func_light(t_lfunc *f);

/*
**		PARSER & TOOLS
*/

void		parse_light(t_env *env, char **line, int fd);
void		parse_obj(t_env *env, char **line, int fd);
void		parse_scene(t_env *env, char **line, int fd);
void		get_cam_matrix(t_cam *cam);
int			ft_open(char *s);
int			ft_close(int fd);
void		fill_primitive_function(t_pfunc *pf);
void		fill_tab_parser(t_func *f);

#endif
