# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vincent <vincent@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/01/03 19:17:39 by vvaleriu          #+#    #+#              #
#    Updated: 2014/03/27 19:21:06 by vvaleriu         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = rt
CC = cc -Wall -Wextra -Werror
#FLAGS = -Wall -Wextra -Werror -g3
I_LINKS = -I./includes -I./libft/includes -I/usr/X11/include
L_LINKS = -L/usr/X11/lib -lmlx -lXext -lX11 -L./libft -lft -lm
RM = rm -rf
VPATH = includes srcs libft 
vpath %.c srcs srcs/calc srcs/draw srcs/init srcs/mvt srcs/tools srcs/matrices \
   srcs/vectors srcs/light
vpath %.h includes libft/includes/
vpath %.a libft
P_OBJ = ./obj/
OBJ = $(SRC:%.c=$(P_OBJ)%.o)
SRC = main.c					\
		init.c					\
		init_ptr.c				\
		parse_light.c			\
		parse_obj.c				\
		parse_scene.c			\
	  	ft_open.c				\
	  	ft_close.c				\
	  	get_next_line.c			\
		key_press_event.c		\
		draw_hook.c				\
		draw_img_pixel.c		\
		expose_window.c			\
		get_obj.c				\
		get_obj_dir.c			\
		get_obj_length.c		\
		get_obj_material.c		\
		get_obj_matrix.c		\
		get_obj_number.c		\
		get_obj_origin.c		\
		get_obj_radius.c		\
		get_obj_type.c			\
		get_light.c				\
		get_light_color.c		\
		get_light_dir.c			\
		get_light_number.c		\
		get_light_origin.c		\
		get_light_type.c		\
		init_ray.c				\
		cal_vp_point.c			\
		cal_pixel.c				\
		cal_intersection.c		\
		cal_hitpos.c			\
		set_virtual_points.c	\
		cal_sphere.c			\
		cal_plan.c				\
		cal_cone.c				\
		cal_rgb.c				\
		cal_light.c				\
		light_general_ambient.c	\
		light_ambient.c			\
		light_diffuse.c			\
		light_specular.c		\
		mat_inverse.c			\
		mat_prod_vec.c			\
		mat_prod_scalar.c		\
		rotate_point.c			\
		translate_point.c		\
		vec_ops.c				\
		vec_prod.c				\
		disp_vec.c				\
		below_one.c				\
		cal_reflected.c			\
		fill_rotation_matrix.c	\
		get_cam_matrix.c		\
		cal_cylinder.c			\
		cpy_vector.c			\

$(NAME): libft.a get_next_line.h rt.h $(OBJ)
	@$(CC) $(FLAGS) -o $@ $(OBJ) $(I_LINKS) $(L_LINKS)
	@echo Compile done

libft.a: libft.h
	@(cd ./libft/ && make)

$(P_OBJ)%.o: %.c
	@(mkdir -p $(P_OBJ))
	@$(CC) -c $(FLAGS) $< -o $@ $(I_LINKS)

all: $(NAME)

clean:
	@$(RM) $(P_OBJ)
	@(cd ./libft/ && make clean)
	@echo "rtv1 objects deleted"

fclean: clean
	@$(RM) $(NAME)
	@(cd ./libft/ && make fclean)
	@echo "binary deleted"

re: fclean all

.PHONY: clean, re
