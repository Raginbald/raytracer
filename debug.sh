#!bin/zsh
cc -g -Wall -Wextra -Werror -o debug ./srcs/light/*.c ./srcs/vectors/*.c ./srcs/matrices/*.c ./srcs/*.c ./srcs/init/*.c ./srcs/mvt/*.c ./srcs/draw/*.c ./srcs/tools/*.c ./srcs/calc/*.c -I./includes -I./libft/includes -L/usr/X11/lib -lmlx -lXext -lX11 -L./libft -lft -lm
