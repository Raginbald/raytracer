/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   light_specular.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/27 02:16:56 by vincent           #+#    #+#             */
/*   Updated: 2014/03/27 16:07:25 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <math.h>

/*
** Calculate the angle between the reflected ray of light and the view ray
** to determine a specular coefficient
*/

void	light_specular(t_ray *ray, t_ray *lt_ray, t_obj *obj, t_light *light)
{
	t_3d	reflected;
	double	scalar;
	double	dist;

	dist = 1 / (1 + lt_ray->d * lt_ray->d * light->ATT);
	cal_reflected(&lt_ray->dir, &ray->norm, &reflected);
	scalar = vec_dotprod(ray->dir, reflected);
	ray->SR += obj->SR * light->SR * pow(scalar, light->SHINE) * dist;
	ray->SG += obj->SG * light->SG * pow(scalar, light->SHINE) * dist;
	ray->SB += obj->SB * light->SB * pow(scalar, light->SHINE) * dist;
}
