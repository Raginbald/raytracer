/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   light_diffuse.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/27 02:16:49 by vincent           #+#    #+#             */
/*   Updated: 2014/03/27 18:14:43 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>

/*
** Diffuse light formula:
** Ird = Iod * Ild * dotprod(light vector, intersection normal)
** Ird : ray diffuse intensity
** Iod : object diffuse intensity
** Ild : light diffuse intensity
*/

void	light_diffuse(t_ray *ray, t_ray *lt_ray, t_obj *obj, t_light *light)
{
	double	scalar;
	double	dist;

	dist = 1 / (1 + lt_ray->d * lt_ray->d * light->ATT);
	scalar = vec_dotprod(ray->norm, lt_ray->dir);
	scalar = (scalar < 0 ? 0 : scalar);
	ray->DR += obj->DR * light->DR * scalar * dist;
	ray->DG += obj->DG * light->DG * scalar * dist;
	ray->DB += obj->DB * light->DB * scalar * dist;
}
