/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   light_general_ambient.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/13 16:18:36 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/25 19:00:10 by vincent          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>

void		light_general_ambient(t_obj *obj, t_ray *ray, t_3d ambient)
{
	ray->AR = obj->AR * ambient.x;
	ray->AG = obj->AG * ambient.z;
	ray->AB = obj->AB * ambient.y;
}
