/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   light_ambient.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/26 22:47:30 by vincent           #+#    #+#             */
/*   Updated: 2014/03/27 18:10:59 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>

void	light_ambient(t_ray *ray, t_obj *obj, t_light *light)
{
	ray->AR += obj->AR * light->AR;
	ray->AG += obj->AG * light->AG;
	ray->AB += obj->AB * light->AB;
}
