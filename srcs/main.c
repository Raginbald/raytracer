/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/20 18:13:25 by vincent           #+#    #+#             */
/*   Updated: 2014/03/27 17:48:26 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <rt.h>
#include <unistd.h>
#include <stdlib.h>
#include <libft.h>

/*
** - e.m : create a matrix from the map given as parameter
** - Initialise e.mlx and create a new window (e.win)
** - key hook : manage keybord events
** - expose hook : manage scene rendering
** - loop : sleep ands shit itself to death
*/

int				main(int ac, char **av)
{
	t_env		e;
	t_pfunc		pf[PRIM_NB];

	if (ac < 2 || ac > 3)
	{
		ft_putendl(USAGE);
		return (0);
	}
	init(&e, av[1], pf);
	init_ptr(&e);
	mlx_expose_hook(e.win, draw_hook, &e);
	mlx_key_hook(e.win, key_press_event, &e);
	mlx_loop(e.mlx);
	return (0);
}
