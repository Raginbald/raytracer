/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cal_hitpos.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/15 14:36:21 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/25 17:33:16 by vincent          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>

/*
** Calculate hit position of the ray after receiving the hit distance:
** - Normalise ray vector
** - Hit position = origin (cam) + (normalised) ray vector * distance (ray->d)
*/

void	cal_hitpos(t_env *e, t_ray *ray)
{
	vec_prod(&(ray->dir), ray->d, &(ray->pos));
	vec_sum(&(ray->pos), &(e->cam.pos), &(ray->pos));
}
