/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cal_intersection.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/14 15:28:30 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/18 22:18:20 by vincent          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>

/*
** Compute ray vector value
*/

t_obj	*cal_intersection(t_env *e, t_ray *ray, t_pfunc *f)
{
	int		i;
	int		nb;
	t_obj	*obj_ret;

	i = 0;
	nb = 0;
	obj_ret = NULL;
	while (nb < e->obj_nb)
	{
		while (i < PRIM_NB)
		{
			if (e->obj[nb].type == f[i].nb)
			{
				set_virtual_points(&(e->cam), ray, &(e->obj[nb]));
				if (f[i].f(e, ray, &(e->obj[nb])))
					obj_ret = &(e->obj[nb]);
			}
			i++;
		}
		i = 0;
		nb++;
	}
	return (obj_ret);
}
