/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_ray.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/14 15:42:38 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/25 13:51:29 by vincent          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>

void	init_ray(t_ray *ray, t_3d *origin, t_3d *point)
{
	vec_sub(point, origin, &(ray->dir));
	ray->d = -1;
	ray->col[0] = 0;
	ray->col[1] = 0;
	ray->col[2] = 0;
	ray->col[3] = 0;
	ray->col[4] = 0;
	ray->col[5] = 0;
	ray->col[6] = 0;
	ray->col[7] = 0;
	ray->col[8] = 0;
	ray->col[9] = 0;
	ray->norm.x = 0;
	ray->norm.y = 0;
	ray->norm.z = 0;
	vec_normalise(&(ray->dir));
}
