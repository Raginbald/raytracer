/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cal_plan.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/14 13:14:22 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 19:01:20 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <math.h>
#include <libft.h>

/*
** Look for an intersection and compute its distance
** If intersection:
**	if distance shorter than previous intersection distance
**		replace distance
**		compute hit position and its normal
**	else
**		nothing
*/

int			cal_plan_inter(t_env *e, t_ray *ray, t_obj *obj)
{
	double	d;

	if (fabs(ray->dir_v.y) > 0.00001)
	{
		d = -e->cam.pos_v.y / ray->dir_v.y;
		if (d > 0)
		{
			if ((ray->d == -1) || (ray->d >= 0 && d < ray->d))
			{
				ray->d = d;
				cal_plan_normal(e, ray, obj);
				return (1);
			}
		}
	}
	return (0);
}

void		cal_plan_normal(t_env *e, t_ray *ray, t_obj *obj)
{
	cal_hitpos(e, ray);
	ray->norm.x = 0;
	ray->norm.y = 1;
	ray->norm.z = 0;
	mat_prod_vec(obj->miz, &(ray->norm), &(ray->norm));
	mat_prod_vec(obj->miy, &(ray->norm), &(ray->norm));
	mat_prod_vec(obj->mix, &(ray->norm), &(ray->norm));
	vec_normalise(&(ray->norm));
}
