/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cal_cylinder.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/14 13:14:22 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 16:29:44 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <math.h>
#include <libft.h>

/*
** Cylinder aligned on the Y axis
**Ray_inter_finite_cylinder(P,d): 
** Check for intersection with infinite cylinder 
** t1,t2 = ray_inter_infinite_cylinder(P,d) 
** compute P + t1*d, P + t2*d 
** If intersection, is it between “end caps”? 
**if y > 1 or y < -1 for t1 or t2, toss it 
** Check for intersection with top end cap 
**Compute ray_inter_plane(t3, plane y = 1) 
**Compute P + t3*d 
** If it intersects, is it within cap circle? 
**if x2 + z2 > 1, toss out t3 
** Check intersection with other end cap 
** Compute ray_inter_plane(t4, plane y = -1) 
** Compute P + t4*d 
** If it intersects, is it within cap circle? 
** if x2 + z2 > 1, toss out t4 
** Among all the t’s that remain (1-4), select the 
** smallest non-negative one
*/

int			cal_cylinder_inter(t_env *e, t_ray *ray, t_obj *obj)
{
	double	det[4];
	double	d_ori;
	
	d_ori = ray->d;
	AA = pow(RDVX, 2) + pow(RDVZ, 2);
	BB = 2 * ((ECPVX * RDVX) + (ECPVZ * RDVZ));
	CC = ECPVX * ECPVX + ECPVZ * ECPVZ - pow(obj->radius, 2);
	DELTA = pow(BB, 2) - (4 * AA * CC);
	if (DELTA >= 0.0001)
		delta_above(e, ray, obj, det);
	check_cap_intersection(e, ray, obj);
	if (d_ori != ray->d)
		return (1);
	return (0);
}

void		delta_above(t_env *e, t_ray *ray, t_obj *obj, double *det)
{
	double	t1;
	double	t2;
	t_3d	tmp = {0, 0, 0};

	t1 = (-BB - sqrt(DELTA)) / (2 * AA);
	t2 = (-BB + sqrt(DELTA)) / (2 * AA);
	t1 = (t1 >= 0 ? t1 : -1);
	t2 = (t2 >= 0 ? t2 : -1);
	t1 = (t1 > t2 ? t2 : t1);
	if (t1 >= 0)
	{
		vec_prod(&(ray->dir_v), t1, &tmp);
		vec_sum(&tmp, &(e->cam.pos_v), &tmp);
		if (tmp.y >= 0 && tmp.y <= obj->len && (ray->d == -1 || (ray->d != -1 && t1 < ray->d)))
		{
			ray->d = t1;
			cpy_vector(&tmp, &(ray->pos));
			cpy_vector(&(ray->pos), &(ray->norm));
			ray->norm.y = 0;
			vec_normalise(&(ray->norm));
			rotate_point_inverse(obj, &(ray->norm), &(ray->norm));
			set_original_points(&(ray->pos), obj);
		}
	}
}

void		check_cap_intersection(t_env *e, t_ray *ray, t_obj *obj)
{
	double		d1;
	double		d2;
	t_3d	tmp = {0, 0, 0};
	t_3d	tmp2 = {0, 0, 0};
	t_3d	norm_t = {0, 0, 0};

	if (fabs(ray->dir_v.y) > 0.000001)
	{
		d1 = -e->cam.pos_v.y / ray->dir_v.y;
		d1 = (d1 > 0.00001 ? d1 : -1);
		d2 = (obj->len - e->cam.pos_v.y) / ray->dir_v.y;
		d2 = (d2 > 0.00001 ? d2 : -1);
		if (d1 > 0.00001)
		{
			tmp.x = e->cam.pos_v.x + ray->dir_v.x * d1;
			tmp.z = e->cam.pos_v.z + ray->dir_v.z * d1;
			d1 = (tmp.x * tmp.x + tmp.z * tmp.z < obj->radius * obj->radius ? d1 : -1);
			if (d1 != -1)
				norm_t.y = -1;
		}
		if (d2 > 0.00001)
		{
			tmp2.x = e->cam.pos_v.x + ray->dir_v.x * d2;
			tmp2.z = e->cam.pos_v.z + ray->dir_v.z * d2;
			d2 = (tmp2.x * tmp2.x + tmp2.z * tmp2.z < (obj->radius * obj->radius)? d2 : -1);
			if ((d2 != -1 && (d2 < d1 && d1 != -1)) || (d2 > 0 && d1 == -1))
			{
				norm_t.y = 1;
				d1 = d2;
			}
		}
		if ((ray->d == -1 || (d1 < ray->d && ray->d != -1)) && d1 != -1)
		{
			ray->d = d1;
			vec_prod(&(ray->dir_v), ray->d, &(ray->pos));
			cpy_vector(&norm_t, &(ray->norm));
			vec_sum(&(ray->pos), &(e->cam.pos_v), &(ray->pos));
			set_original_points(&(ray->pos), obj);
			rotate_point_inverse(obj, &(ray->norm), &(ray->norm));
		}
	}
}
