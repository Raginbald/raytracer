/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cal_sphere.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/14 13:14:22 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/25 16:57:12 by vincent          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <math.h>
#include <libft.h>

/*
** Look for an intersection and compute its distance
** If intersection:
**	if distance shorter than previous intersection distance
**		replace distance
**		compute hit position and its normal
**	else
**		nothing
*/

int			cal_sphere_inter(t_env *e, t_ray *ray, t_obj *obj)
{
	double	a;
	double	b;
	double	c;
	double	delta;

	a = pow(RDVX, 2) + pow(RDVY, 2) + pow(RDVZ, 2);
	b = 2 * ((ECPVX * RDVX) + (ECPVY * RDVY) + (ECPVZ * RDVZ));
	c = ECPVX * ECPVX + ECPVY * ECPVY + ECPVZ * ECPVZ - pow(obj->radius, 2);
	delta = pow(b, 2) - (4 * a * c);
	if (delta >= 0.0001)
	{
		c = (-b - sqrt(delta)) / (2 * a);
		delta = (-b + sqrt(delta)) / (2 * a);
		delta = (delta < c ? delta : c);
		if ((ray->d == -1) || (ray->d >= 0 && delta < ray->d))
		{
			ray->d = delta;
			cal_sphere_normal(e, ray, obj);
			return (1);
		}
	}
	return (0);
}

void		cal_sphere_normal(t_env *e, t_ray *ray, t_obj *obj)
{
	cal_hitpos(e, ray);
	vec_sub(&(ray->pos), &(obj->pos), &(ray->norm));
	vec_normalise(&(ray->norm));
}
