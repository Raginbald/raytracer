/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cal_reflected.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/15 11:31:12 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 18:51:50 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>

/*
** Reflected ray computation formula:
** refelected = 2 * (norm * inc) * norm - inc
*/

void	cal_reflected(t_3d *inc, t_3d *norm, t_3d *reflected)
{
	vec_multiply(inc, norm, reflected);
	vec_multiply(reflected, norm, reflected);
	vec_prod(reflected, 2, reflected);
	vec_sub(reflected, inc, reflected);
}
