/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cal_rgb.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/13 18:15:02 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 18:47:28 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>

int		cal_rgb(double *col)
{
	int		r;
	int		g;
	int		b;
	int		ret;

	ret = 0;
	AR = AR + DR + SR;
	AG = AG + DG + SG;
	AB = AB + DB + SB;
	r = (int)(255 * AR);
	g = (int)(255 * AG);
	b = (int)(255 * AB);
	r = (r <= 255 ? r : 255);
	g = (g <= 255 ? g : 255);
	b = (b <= 255 ? b : 255);
	ret = r;
	ret <<= 8;
	ret += g;
	ret <<= 8;
	ret += b;
	return (ret);
}
