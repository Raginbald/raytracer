/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cal_light.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/16 17:14:45 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 19:00:48 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>

/*
** - Calculer le vecteur light (lt_vec): vecteur partant de la source de lumiere
** jusqu'au point d'intersection avec du rayon avec l'objet
** - Normaliser ce vecteur
** - Verifier si ce vecteur rencontre un autre obj sur son chemin pour savoir
** si c'est une ombre ou pas
** - Si ce n'est pas une ombre, calculer l'angle de frappe: angle du rayon
** lumineux avec la suface en utilisant la normale au point d'intersection.
** Si angle <=0 alors couleur finale = couleur de fond.
** Sinon finale = mdiffusion * couleur lumiere * angle
** Il faut donc calculer la normale au point d'intersection
** lt_ray	: intersection to light vector
** lt_nb	: number of current light
** rfl		: reflection vector
*/

void		cal_light(t_env *e, t_ray *ray, t_obj *obj, t_pfunc *pf)
{
	int		lt_nb;
	double	dist;
	t_ray	lt_ray;

	lt_nb = 0;
	light_general_ambient(obj, ray, e->ambient);
	while (lt_nb < e->light_nb)
	{
		init_lightray(ray, &lt_ray, &(e->light[lt_nb]));
		light_ambient(ray, obj, &(e->light[lt_nb]));
		dist = lt_ray.d;
		lt_ray.d = -1;
		if (!cal_intersection(e, &(lt_ray), pf) || (dist < lt_ray.d))
		{
			light_diffuse(ray, &lt_ray, obj, &(e->light[lt_nb]));
			light_specular(ray, &lt_ray, obj, &(e->light[lt_nb]));
		}
		lt_nb++;
	}
}

void		init_lightray(t_ray *ray, t_ray *lt_ray, t_light *light)
{
	vec_sub(&(light->pos), &(ray->pos), &(lt_ray->dir));
	lt_ray->d = vec_norm(lt_ray->dir);
	vec_normalise(&(lt_ray->dir));
}
