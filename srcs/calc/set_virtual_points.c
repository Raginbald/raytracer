/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_virtual_points.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/14 16:54:34 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 18:45:10 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>

/*
** Set cam->virtual position by translation of the cam origin
** Set ray->virtual vector by rotation of the original vector
*/

void	set_virtual_points(t_cam *cam, t_ray *ray, t_obj *obj)
{
	translate_point(&(obj->pos), &(cam->pos), &(cam->pos_v));
	rotate_point(obj, &(ray->dir), &(ray->dir_v));
}

void	set_original_points(t_3d *p, t_obj *obj)
{
	rotate_point_inverse(obj, p, p);
	translate_point_inverse(&(obj->pos), p, p);
}
