/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cal_pixel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/14 15:28:30 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 18:57:17 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>

/*
** Compute ray vector value
** Look for intersection
** if no intersection found : return black color.
** lray: lray computed if intersection found
*/

int		cal_pixel(t_env *e, t_ray *ray, t_pfunc *pf, int depth)
{
	t_obj		*obj;

	obj = NULL;
	if (depth == DEPTH_MAX)
		return (cal_rgb(ray->col));
	if ((obj = cal_intersection(e, ray, pf)) == NULL)
		return (0);
	cal_light(e, ray, obj, pf);
	return (cal_rgb(ray->col));
}
