/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cal_vp_point.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/18 10:46:04 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 18:47:06 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>

/*
** Compute the coordonnate of the required point on the viewplane to throw a
** ON calcule le point sur le viewplane comme si la cam etait placee en 0 0 0
** On applique une translation au point obtenu
** On applique une rotation au poit obtenu ce qui donne finalement le point sur
** le viewplane
*/

void	cal_vp_point(t_cam *cam, t_3d *p, t_2d_coord t)
{
	p->z = V_DIST;
	p->x = -cam->xres / 2 + X;
	p->y = cam->yres / 2 - Y;
	p->x = p->x + cam->pos.x;
	p->y = p->y + cam->pos.y;
	p->z = p->z + cam->pos.z;
	mat_prod_vec(cam->mx, p, p);
	mat_prod_vec(cam->my, p, p);
	mat_prod_vec(cam->mz, p, p);
}
