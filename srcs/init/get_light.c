/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_light.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vvaleriu <vvaleriu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/15 18:20:57 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/13 12:54:46 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>
#include <get_next_line.h>

void		get_light(t_light *light, char **line, int fd)
{
	char	*tmp;
	t_lfunc	f[4];
	int		i;

	tmp = *line;
	i = 0;
	fill_func_light(f);
	while (*tmp != '}')
	{
		tmp = *line;
		while (ft_isspace(*tmp))
			tmp++;
		while (i < 4)
		{
			if (!ft_strncmp(tmp, f[i].s, ft_strlen(f[i].s)))
				f[i].f(light, tmp);
			i++;
		}
		i = 0;
		get_next_line(fd, line);
		tmp = *line;
		while (ft_isspace(*tmp))
			tmp++;
	}
}

void		fill_func_light(t_lfunc *f)
{
	f[0].s = "type";
	f[0].f = get_light_type;
	f[1].s = "origin";
	f[1].f = get_light_origin;
	f[2].s = "color";
	f[2].f = get_light_color;
	f[3].s = "dir";
	f[3].f = get_light_dir;
}
