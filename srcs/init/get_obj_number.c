/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_obj_number.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/11 01:11:32 by vincent           #+#    #+#             */
/*   Updated: 2014/03/27 18:29:27 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>
#include <get_next_line.h>

void		get_obj_number(t_env *env, char *s)
{
	while (!ft_isdigit(*s))
		s++;
	env->obj_nb = ft_atoi(s);
	env->obj = (t_obj *)ft_memalloc(sizeof(t_obj) * env->obj_nb);
}
