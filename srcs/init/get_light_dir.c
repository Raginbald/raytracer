/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_light_dir.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/16 17:23:29 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 15:57:39 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>
#include <get_next_line.h>

void		get_light_dir(t_light *light, char *s)
{
	char	**split;

	while (!ft_isdigit(*s) && *s != '-')
		s++;
	split = ft_strsplit(s, ' ');
	light->dir.x = ft_atod(split[0]);
	light->dir.y = ft_atod(split[1]);
	light->dir.z = ft_atod(split[2]);
	free(split);
}
