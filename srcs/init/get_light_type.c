/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_light_type.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vvaleriu <vvaleriu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/16 17:24:14 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/02/16 17:24:21 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>

void		get_light_type(t_light *light, char *s)
{
	while (!ft_isdigit(*s))
		s++;
	light->type = ft_atoi(s);
}
