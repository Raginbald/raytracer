/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_obj.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vvaleriu <vvaleriu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/11 11:02:54 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/12 18:00:25 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>
#include <get_next_line.h>

void			parse_obj(t_env *env, char **line, int fd)
{
	char	*tmp;
	int		j;

	j = 0;
	tmp = *line;
	while (ft_strcmp("__END_OBJECT", *line))
	{
		while (ft_isspace(*tmp))
			tmp++;
		if (!ft_strncmp(tmp, "number", ft_strlen("number")))
			get_obj_number(env, tmp);
		if (!ft_strncmp(tmp, "obj", ft_strlen("obj")))
		{
			get_obj(&(env->obj[j]), line, fd);
			j++;
		}
		get_next_line(fd, line);
		tmp = *line;
	}
}
