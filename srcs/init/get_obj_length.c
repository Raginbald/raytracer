/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_obj_length.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vvaleriu <vvaleriu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/12 18:03:59 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/13 11:27:32 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>

void		get_obj_length(t_obj *obj, char *s)
{
	while (!ft_isdigit(*s))
		s++;
	obj->len = ft_atod(s);
}
