/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/11 11:03:00 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 18:30:00 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>
#include <math.h>
#include <get_next_line.h>

/*
** Parse file to store scene info
** Initialise pf a pointer to primitive calculation function array
*/

void		init(t_env *e, char *arg, t_pfunc *pf)
{
	int		fd;
	char	*line;
	t_func	f[3];
	int		i;

	i = 0;
	line = NULL;
	fd = ft_open(arg);
	fill_tab_parser(f);
	get_next_line(fd, &line);
	while (ft_strcmp(line, "__END"))
	{
		while (i < 3)
		{
			if (!ft_strcmp(line, f[i].s))
				f[i].f(e, &line, fd);
			i++;
		}
		i = 0;
		get_next_line(fd, &line);
	}
	ft_close(fd);
	fill_primitive_function(pf);
	e->pf = (void *)pf;
}

void		fill_tab_parser(t_func *f)
{
	f[0].s = "__SCENE";
	f[0].f = parse_scene;
	f[1].s = "__OBJECT";
	f[1].f = parse_obj;
	f[2].s = "__LIGHT";
	f[2].f = parse_light;
}

void		fill_primitive_function(t_pfunc *pf)
{
	pf[0].nb = 0;
	pf[0].f = cal_sphere_inter;
	pf[1].nb = 1;
	pf[1].f = cal_plan_inter;
	pf[2].nb = 2;
	pf[2].f = cal_cylinder_inter;
	pf[3].nb = 3;
	pf[3].f = cal_cone_inter;
}
