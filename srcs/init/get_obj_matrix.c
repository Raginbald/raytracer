/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_obj_matrix.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/13 14:11:00 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 18:29:16 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <math.h>

/*
** Create roration matrices and inverse matrice for each objet
*/

void			get_obj_matrix(t_obj *obj)
{
	init_mat(&(obj->mx));
	init_mat(&(obj->my));
	init_mat(&(obj->mz));
	init_mat(&(obj->mix));
	init_mat(&(obj->miy));
	init_mat(&(obj->miz));
	rot_x_matrix(&(obj->mx), obj->dir);
	rot_y_matrix(&(obj->my), obj->dir);
	rot_z_matrix(&(obj->mz), obj->dir);
	mat_inverse(&(obj->mx), &(obj->mix));
	mat_inverse(&(obj->my), &(obj->miy));
	mat_inverse(&(obj->mz), &(obj->miz));
}
