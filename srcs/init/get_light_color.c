/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_light_color.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/13 12:44:15 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 15:57:23 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>

void			get_light_color(t_light *light, char *s)
{
	char	**t;

	while (!ft_isdigit(*s))
		s++;
	t = ft_strsplit(s, ' ');
	light->AR = below_one(ft_atod(t[0]));
	light->AG = below_one(ft_atod(t[1]));
	light->AB = below_one(ft_atod(t[2]));
	light->DR = below_one(ft_atod(t[3]));
	light->DG = below_one(ft_atod(t[4]));
	light->DB = below_one(ft_atod(t[5]));
	light->SR = below_one(ft_atod(t[6]));
	light->SG = below_one(ft_atod(t[7]));
	light->SB = below_one(ft_atod(t[8]));
	light->SHINE = below_one(ft_atod(t[9]));
	light->ATT = below_one(ft_atod(t[10]));
	free(t);
}
