/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_obj.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/11 01:12:52 by vincent           #+#    #+#             */
/*   Updated: 2014/03/18 09:20:13 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>
#include <get_next_line.h>

void		get_obj(t_obj *obj, char **line, int fd)
{
	char	*tmp;
	t_ofunc	f[6];
	int		i;

	tmp = *line;
	i = 0;
	fill_func_obj(f);
	while (*tmp != '}')
	{
		tmp = *line;
		while (ft_isspace(*tmp))
			tmp++;
		while (i < 6)
		{
			if (!ft_strncmp(tmp, f[i].s, ft_strlen(f[i].s)))
				f[i].f(obj, tmp);
			i++;
		}
		i = 0;
		get_next_line(fd, line);
		tmp = *line;
		while (ft_isspace(*tmp))
			tmp++;
	}
	get_obj_matrix(obj);
}

void		fill_func_obj(t_ofunc *f)
{
	f[0].s = "type";
	f[0].f = get_obj_type;
	f[1].s = "origin";
	f[1].f = get_obj_origin;
	f[2].s = "material";
	f[2].f = get_obj_material;
	f[3].s = "radius";
	f[3].f = get_obj_radius;
	f[4].s = "length";
	f[4].f = get_obj_length;
	f[5].s = "dir";
	f[5].f = get_obj_dir;
}
