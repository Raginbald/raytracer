/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_obj_origin.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/11 01:14:00 by vincent           #+#    #+#             */
/*   Updated: 2014/03/27 16:01:13 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>
#include <get_next_line.h>
#include <stdlib.h>

void		get_obj_origin(t_obj *obj, char *s)
{
	char	**split;

	while (!ft_isdigit(*s) && *s != '-')
		s++;
	split = ft_strsplit(s, ' ');
	obj->pos.x = ft_atod(split[0]);
	obj->pos.y = ft_atod(split[1]);
	obj->pos.z = ft_atod(split[2]);
	free(split);
}
