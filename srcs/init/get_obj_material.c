/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_obj_material.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/13 12:13:33 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 16:00:26 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>
#include <stdlib.h>

void			get_obj_material(t_obj *obj, char *s)
{
	char	**t;

	while (!ft_isdigit(*s))
		s++;
	t = ft_strsplit(s, ' ');
	obj->AR = below_one(ft_atod(t[0]));
	obj->AG = below_one(ft_atod(t[1]));
	obj->AB = below_one(ft_atod(t[2]));
	obj->DR = below_one(ft_atod(t[3]));
	obj->DG = below_one(ft_atod(t[4]));
	obj->DB = below_one(ft_atod(t[5]));
	obj->SR = below_one(ft_atod(t[6]));
	obj->SG = below_one(ft_atod(t[7]));
	obj->SB = below_one(ft_atod(t[8]));
	obj->SHINE = below_one(ft_atod(t[9]));
	free(*t);
}
