/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_light_origin.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/16 17:23:59 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 15:58:37 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>
#include <get_next_line.h>

void		get_light_origin(t_light *light, char *s)
{
	char	**split;

	while (!ft_isdigit(*s) && *s != '-')
		s++;
	split = ft_strsplit(s, ' ');
	light->pos.x = ft_atod(split[0]);
	light->pos.y = ft_atod(split[1]);
	light->pos.z = ft_atod(split[2]);
	free(split);
}
