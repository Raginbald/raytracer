/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_light_number.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vvaleriu <vvaleriu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/16 17:23:43 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 18:28:54 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>
#include <get_next_line.h>

void		get_light_number(t_env *env, char *s)
{
	while (!ft_isdigit(*s))
		s++;
	env->light_nb = ft_atoi(s);
	env->light = (t_light *)ft_memalloc(sizeof(t_light) * env->light_nb);
}
