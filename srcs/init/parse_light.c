/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_light.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vvaleriu <vvaleriu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/15 18:20:12 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/02/15 18:20:25 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>
#include <get_next_line.h>

void	parse_light(t_env *env, char **line, int fd)
{
	char	*tmp;
	int		j;

	j = 0;
	tmp = *line;
	while (ft_strcmp("__END_LIGHT", *line))
	{
		while (ft_isspace(*tmp))
			tmp++;
		if (!ft_strncmp(tmp, "number", ft_strlen("number")))
			get_light_number(env, tmp);
		if (!ft_strncmp(tmp, "light", ft_strlen("light")))
		{
			get_light(&(env->light[j]), line, fd);
			j++;
		}
		get_next_line(fd, line);
		tmp = *line;
	}
}
