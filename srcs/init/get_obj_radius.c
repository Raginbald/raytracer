/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_obj_radius.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vvaleriu <vvaleriu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/13 15:24:25 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/13 11:27:02 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>

void		get_obj_radius(t_obj *obj, char *s)
{
	while (!ft_isdigit(*s))
		s++;
	obj->radius = ft_atod(s);
}
