/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_open.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vvaleriu <vvaleriu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/21 13:47:00 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/01/04 13:19:09 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <stdlib.h>
#include <libft.h>

int		ft_open(char *s)
{
	int		fd;

	fd = open(s, O_RDONLY);
	if (fd == -1)
	{
		ft_putendl("file can't be opened");
		exit(1);
	}
	return (fd);
}
