/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_obj_dir.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/13 11:29:37 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 15:59:14 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>
#include <get_next_line.h>

void		get_obj_dir(t_obj *obj, char *s)
{
	char	**split;

	while (!ft_isdigit(*s) && *s != '-')
		s++;
	split = ft_strsplit(s, ' ');
	obj->dir.x = ft_pitorad(ft_atod(split[0]));
	obj->dir.y = ft_pitorad(ft_atod(split[1]));
	obj->dir.z = ft_pitorad(ft_atod(split[2]));
	free(split);
}
