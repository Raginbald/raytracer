/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_scene.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/16 17:29:43 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 18:30:21 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>
#include <get_next_line.h>
#include <math.h>

void		parse_scene(t_env *env, char **line, int fd)
{
	char	*tmp;

	tmp = *line;
	while (ft_strcmp(tmp, "__END_SCENE"))
	{
		while ((ft_isspace(*tmp)))
			tmp++;
		if (!ft_strncmp(tmp, "origin", 6))
			get_cam_origin(env, tmp);
		if (!ft_strncmp(tmp, "dir", 3))
			get_cam_dir(env, tmp);
		if (!ft_strncmp(tmp, "res", 3))
			get_res(env, tmp);
		if (!ft_strncmp(tmp, "ambient", 7))
			get_ambient(env, tmp);
		get_next_line(fd, line);
		tmp = *line;
	}
	get_cam_matrix(&(env->cam));
}

void		get_cam_origin(t_env *env, char *s)
{
	char	**split;

	while (!ft_isdigit(*s) && *s != '-')
		s++;
	split = ft_strsplit(s, ' ');
	env->cam.pos.x = ft_atod(split[0]);
	env->cam.pos.y = ft_atod(split[1]);
	env->cam.pos.z = ft_atod(split[2]);
	free(*split);
}

void		get_cam_dir(t_env *env, char *s)
{
	char	**split;

	while (!ft_isdigit(*s) && *s != '-')
		s++;
	split = ft_strsplit(s, ' ');
	env->cam.dir.x = ft_pitorad(ft_atod(split[0]));
	env->cam.dir.y = ft_pitorad(ft_atod(split[1]));
	env->cam.dir.z = ft_pitorad(ft_atod(split[2]));
	free(*split);
}

void		get_res(t_env *env, char *s)
{
	char	**split;

	while (!ft_isdigit(*s))
		s++;
	split = ft_strsplit(s, ' ');
	env->cam.xres = ft_atoi(split[0]);
	env->cam.yres = ft_atoi(split[1]);
	env->cam.v_dist = env->cam.xres / (2 * tan(0.575958653));
	free(*split);
}

void		get_ambient(t_env *env, char *s)
{
	char	**split;

	while (!ft_isdigit(*s) && *s != '-')
		s++;
	split = ft_strsplit(s, ' ');
	env->ambient.x = below_one(ft_atod(split[0]));
	env->ambient.y = below_one(ft_atod(split[1]));
	env->ambient.z = below_one(ft_atod(split[2]));
	free(split);
}
