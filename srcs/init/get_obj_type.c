/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_obj_type.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/11 01:13:17 by vincent           #+#    #+#             */
/*   Updated: 2014/03/12 18:03:16 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <libft.h>

void		get_obj_type(t_obj *obj, char *s)
{
	while (!ft_isdigit(*s))
		s++;
	obj->type = ft_atoi(s);
}
