/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_ptr.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vvaleriu <vvaleriu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/16 17:29:25 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/02/16 17:29:26 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <mlx.h>

void	init_ptr(t_env *e)
{
	e->mlx = mlx_init();
	e->img_ptr = mlx_new_image(e->mlx, (e->cam).xres, (e->cam).yres);
	e->win = mlx_new_window(e->mlx, (e->cam).xres, (e->cam).yres, W_NAME);
	e->img = mlx_get_data_addr(e->img_ptr, &e->bpp, &e->size_line, &e->endian);
}
