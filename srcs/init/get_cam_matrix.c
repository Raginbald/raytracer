/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_cam_matrix.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/13 14:11:00 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 02:49:49 by vincent          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <math.h>

void			get_cam_matrix(t_cam *cam)
{
	init_mat(&(cam->mx));
	init_mat(&(cam->my));
	init_mat(&(cam->mz));
	rot_x_matrix(&(cam->mx), cam->dir);
	rot_y_matrix(&(cam->my), cam->dir);
	rot_z_matrix(&(cam->mz), cam->dir);
}
