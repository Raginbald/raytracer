/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   translate_point.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/14 16:22:02 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 17:55:31 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>

void	translate_point(t_3d *ori, t_3d *point, t_3d *new)
{
	new->x = point->x - ori->x;
	new->y = point->y - ori->y;
	new->z = point->z - ori->z;
}

void	translate_point_inverse(t_3d *ori, t_3d *point, t_3d *new)
{
	new->x = point->x + ori->x;
	new->y = point->y + ori->y;
	new->z = point->z + ori->z;
}
