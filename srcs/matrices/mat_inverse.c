/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mat_inverse.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/24 19:03:49 by vincent           #+#    #+#             */
/*   Updated: 2014/03/27 18:01:56 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>

/*
** m: original matrix
** i: inverted matrix to be computed
*/

void	mat_calb(t_mat4 *b, t_mat4 *a);
double	mat_determinant(t_mat4 *m);

void	mat_inverse(t_mat4 *m, t_mat4 *i)
{
	double	det;
	t_mat4	b;

	det = mat_determinant(m);
	mat_calb(&b, m);
	mat_prod_scalar(&b, 1 / det, i);
}

double	mat_determinant(t_mat4 *m)
{
	return (m->M03 * m->M12 * m->M21 * m->M30 - m->M02 * m->M13 * m->M21 \
	* m->M30 - m->M03 * m->M11 * m->M22 * m->M30 + m->M01 * m->M13 * m->M22 *\
	m->M30 + m->M02 * m->M11 * m->M23 * m->M30 - m->M01 * m->M12 * m->M23 *\
	m->M30 - m->M03 * m->M12 * m->M20 * m->M31 + m->M02 * m->M13 * m->M20 \
	* m->M31 + m->M03 * m->M10 * m->M22 * m->M31 - m->M00 * m->M13 * m->M22 \
	* m->M31 - m->M02 * m->M10 * m->M23 * m->M31 + m->M00 * m->M12 * m->M23 \
	* m->M31 + m->M03* m->M11 * m->M20 * m->M32 - m->M01 * m->M13 * m->M20 \
	* m->M32 - m->M03 * m->M10 * m->M21 * m->M32 + m->M00 * m->M13 * m->M21 \
	* m->M32 + m->M01 * m->M10 * m->M23 * m->M32 - m->M00 * m->M11 * m->M23 \
	* m->M32 - m->M02 * m->M11 * m->M20 * m->M33 + m->M01 * m->M12 * m->M20 \
	* m->M33 + m->M02 * m->M10 * m->M21 * m->M33 - m->M00 * m->M12 * m->M21 \
	* m->M33 - m->M01 * m->M10 * m->M22 * m->M33 + m->M00 * m->M11 * m->M22 \
	* m->M33);
}

/*
** calculate the B matrix, necessary to compute the inverse of a
*/

void	mat_calb(t_mat4 *b, t_mat4 *a)
{
	b->M00 = L11 L12 L13
	b->M01 = L21 L22 L23
	b->M02 = L31 L32 L33
	b->M03 = L41 L42 L43
	b->M10 = L51 L52 L53
	b->M11 = L61 L62 L63
	b->M12 = L71 L72 L73
	b->M13 = L81 L82 L83
	b->M20 = L91 L92 L93
	b->M21 = L101 L102 L103
	b->M22 = L111 L112 L113
	b->M23 = L121 L122 L123
	b->M30 = L131 L132 L133
	b->M31 = L141 L142 L143
	b->M32 = L151 L152 L153
	b->M33 = L161 L162 L163
}
