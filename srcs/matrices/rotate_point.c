/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate_point.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/18 09:28:31 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 17:55:57 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>

void	rotate_point(t_obj *obj, t_3d *ori, t_3d *new)
{
	mat_prod_vec(obj->mx, ori, new);
	mat_prod_vec(obj->my, new, new);
	mat_prod_vec(obj->mz, new, new);
}

void	rotate_point_inverse(t_obj *obj, t_3d *ori, t_3d *new)
{
	mat_prod_vec(obj->miz, ori, new);
	mat_prod_vec(obj->miy, new, new);
	mat_prod_vec(obj->mix, new, new);
}
