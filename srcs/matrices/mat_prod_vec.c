/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mat_prod_vec.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/24 19:03:49 by vincent           #+#    #+#             */
/*   Updated: 2014/03/27 02:20:12 by vincent          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>

/*
** take a matrix, coordonnatees from a point v and apply a transformation
** to store the new coordonnates into r
*/

void	mat_prod_vec(t_mat4 m, t_3d *v, t_3d *r)
{
	t_3d	tmp;

	tmp.x = v->x;
	tmp.y = v->y;
	tmp.z = v->z;
	r->x = (m.M00 * tmp.x) + (m.M01 * tmp.y) + (m.M02 * tmp.z) + m.M03;
	r->y = (m.M10 * tmp.x) + (m.M11 * tmp.y) + (m.M12 * tmp.z) + m.M13;
	r->z = (m.M20 * tmp.x) + (m.M21 * tmp.y) + (m.M22 * tmp.z) + m.M23;
}
