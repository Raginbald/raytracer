/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_rotation_matrix.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/13 14:11:00 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 17:58:47 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <math.h>

void		rot_x_matrix(t_mat4 *m, t_3d ang)
{
	m->M00 = 1;
	m->M11 = (fabs(cos(ang.x)) > 0.0000001 ? cos(ang.x) : 0);
	m->M12 = (fabs(-sin(ang.x)) > 0.0000001 ? -sin(ang.x) : 0);
	m->M21 = (fabs(sin(ang.x)) > 0.0000001 ? sin(ang.x) : 0);
	m->M22 = (fabs(cos(ang.x)) > 0.0000001 ? cos(ang.x) : 0);
	m->M33 = 1;
}

void		rot_y_matrix(t_mat4 *m, t_3d ang)
{
	m->M00 = (fabs(cos(ang.y)) > 0.00001 ? cos(ang.y) : 0);
	m->M02 = (fabs(sin(ang.y)) > 0.00001 ? sin(ang.y) : 0);
	m->M11 = 1;
	m->M20 = (fabs(-sin(ang.y)) > 0.00001 ? -sin(ang.y) : 0);
	m->M22 = (fabs(cos(ang.y)) > 0.00001 ? cos(ang.y) : 0);
	m->M33 = 1;
}

void		rot_z_matrix(t_mat4 *m, t_3d ang)
{
	m->M00 = (fabs(cos(ang.z)) > 0.00001 ? cos(ang.z) : 0);
	m->M01 = (fabs(-sin(ang.z)) > 0.00001 ? -sin(ang.z) : 0);
	m->M10 = (fabs(sin(ang.z)) > 0.00001 ? sin(ang.z) : 0);
	m->M11 = (fabs(cos(ang.z)) > 0.00001 ? cos(ang.z) : 0);
	m->M22 = 1;
	m->M33 = 1;
}

void		init_mat(t_mat4 *t)
{
	int	i;

	i = 0;
	while (i < 16)
	{
		t->m[i] = 0;
		i++;
	}
}
