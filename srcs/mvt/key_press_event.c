/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_press_event.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/17 13:12:34 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/02/15 12:56:04 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <stdlib.h>

int		key_press_event(int keycode, t_env *e)
{
	if (e != NULL)
	{
		if (keycode == 65307)
			exit(0);
	}
	return (0);
}
