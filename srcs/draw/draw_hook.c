/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_hook.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/14 15:13:41 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 18:17:57 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <stdlib.h>
#include <libft.h>
#include <unistd.h>

/*
** == insert movement or recalculation functions
** For each x and y compute the color the pixel ==
** ray		: ray casted
** *obj		: pointer to the object intersecting the ray
** vpl	: point of the view plane used to cast a ray
*/

int		draw_hook(t_env *e)
{
	t_2d_coord	t;
	t_ray		ray;
	t_3d		vpl;
	t_pfunc		*pf;

	t.x = 0;
	t.y = 0;
	pf = (t_pfunc *)e->pf;
	while (X < e->cam.xres)
	{
		while (Y < e->cam.yres)
		{
			cal_vp_point(&(e->cam), &vpl, t);
			init_ray(&ray, &(e->cam.pos), &vpl);
			draw_img_pixel(e, X, Y, cal_pixel(e, &ray, pf, 0));
			Y++;
		}
		Y = 0;
		X++;
	}
	expose_window(e);
	return (1);
}
