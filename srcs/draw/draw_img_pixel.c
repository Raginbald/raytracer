/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_img_pixel.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/10 16:15:33 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/02/12 19:07:47 by vincent          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>

/*
** Fill every pixel of the screen in the image file
*/

void	draw_img_pixel(t_env *e, int x, int y, int color)
{
	int		pix;

	pix = y * e->size_line + x * (e->bpp / 8);
	e->img[pix] = color;
	e->img[pix + 1] = color >> 8;
	e->img[pix + 2] = color >> 16;
}
