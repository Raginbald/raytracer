/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec_prod.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/14 19:02:54 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/27 18:20:46 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>

/*
** - Dot prod / produit scalaire
** !!! Need normalised vectors !!
** give the cos(angle) between 2 vectors
** - Cross product / produit vectoriel
** used to find a perpendicular vector to the 2 others
*/

double		vec_dotprod(t_3d a, t_3d b)
{
	return ((a.x * b.x) + (a.y * b.y) + (a.z * b.z));
}

void		vec_crossprod(t_3d *a, t_3d *b, t_3d *c)
{
	c->x = (a->y * b->z) - (a->z * b->y);
	c->y = (a->z * b->x) - (a->x * b->z);
	c->z = (a->x * b->y) - (a->y * b->x);
}

void		vec_prod(t_3d *a, double i, t_3d *c)
{
	c->x = (a->x * i);
	c->y = (a->y * i);
	c->z = (a->z * i);
}

void		vec_multiply(t_3d *a, t_3d *b, t_3d *c)
{
	c->x = a->x * b->x;
	c->y = a->y * b->y;
	c->z = a->z * b->z;
}
