/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec_ops.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vvaleriu <vvaleriu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/14 18:34:40 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/03/14 15:25:09 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <math.h>

void	vec_sum(t_3d *p1, t_3d *p2, t_3d *p3)
{
	p3->x = p1->x + p2->x;
	p3->y = p1->y + p2->y;
	p3->z = p1->z + p2->z;
}

void	vec_sub(t_3d *p1, t_3d *p2, t_3d *p3)
{
	p3->x = p1->x - p2->x;
	p3->y = p1->y - p2->y;
	p3->z = p1->z - p2->z;
}

double	vec_norm(t_3d v)
{
	return (sqrt(v.x * v.x + v.y * v.y + v.z * v.z));
}

void	vec_normalise(t_3d *v)
{
	double	len;

	len = vec_norm(*v);
	v->x = v->x / len;
	v->y = v->y / len;
	v->z = v->z / len;
}
