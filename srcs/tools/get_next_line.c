/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/02 19:02:35 by vincent           #+#    #+#             */
/*   Updated: 2014/03/27 17:54:17 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <get_next_line.h>
#include <libft.h>
#include <unistd.h>

char			*ft_realloc(char *ptr, size_t len)
{
	char	*new;

	if (!(new = (char *)malloc(sizeof(char *) * len)))
		return (NULL);
	ft_bzero(new, len);
	ft_memcpy(new, ptr, len);
	free(ptr);
	return (new);
}

static int		ft_get_buf(int const fd, t_counter *i, char **line, char *buf)
{
	int			j;

	j = 1;
	while (!(i->ret < BUFF_SIZE || ft_strnstr(buf + i->i, "\n", BUFF_SIZE - \
					i->i) != 0) || i->i >= BUFF_SIZE)
	{
		ft_strncat(*line, buf + i->i, i->ret - i->i);
		i->i = 0;
		j++;
		if (!(*line = ft_realloc(*line, j * BUFF_SIZE + 1)))
			return (-1);
		ft_bzero(buf, BUFF_SIZE);
		if ((i->ret = read(fd, buf, BUFF_SIZE)) == -1)
			return (-1);
	}
	return (0);
}

int					get_next_line(int const fd, char **line)
{
	static t_counter	i = {0, 0};
	static char			buf[BUFF_SIZE] = {0};

	if (!(*line = (char *)malloc(sizeof(char) * BUFF_SIZE + 1)))
		return (-1);
	if (1 + (**line = 0) && !(*buf))
	{
		if ((i.ret = read(fd, buf, BUFF_SIZE)) == -1)
			return (-1);
	}
	if ((ft_get_buf(fd, &i, line, buf)) == -1)
		return (-1);
	if (ENDL)
	{
		ft_strncat(*line, buf + i.i, ENDL - buf - i.i);
		i.i = ENDL - buf + 1;
	}
	else if (i.ret >= 1 && i.i != i.ret)
	{
		ft_strncat(*line, buf + i.i, ENDL - buf - i.i);
		i.i = i.ret;
	}
	else if (1 + (i.ret = i.i = 0))
		ft_bzero(buf, BUFF_SIZE);
	return ((i.ret >= 1 ? 1 : i.ret));
}
